const express = require ('express')
const router = express.Router()
const courseController = require("../controllers/course")
const auth = require("../auth")

//route to create a new course
//auth.verify below is what is called "Middleware"
router.post("/", auth.verify, (req, res) => {
	//Activity instructions:
	//Create a route + controller function to create a new course,
	//complete with a proper response from the server once the operation resolves
	//Make sure to use postman to send a request to test/create your course

	//auth.decode turns out token into a decoded JavaScript object that we can use the dot notation on to access its properties (such as isAdmin)

	if(auth.decode(req.headers.authorization).isAdmin){
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	}else{
		res.send({auth: "failed"})
	}
})

//route for getting all active courses

router.get("/", (req,res) => {
	console.log(req)
	courseController.getCourses().then(resultFromController => res.send (
		resultFromController))
})

//route for getting a single course

router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send (
		resultFromController))
})

//update existing course

router.put("/:courseId", auth.verify, (req, res) =>{
	if(auth.decode(req.headers.authorization).isAdmin) {
		courseController.updateCourse(req.params, req.body).then(resultFromController => res.send (
		resultFromController))
	}else{
		res.send({auth: "failed"})
	}
})

router.delete("/:courseId", auth.verify, (req, res) => {
	/*
		Activity:
		Create a route for archiving a specific course with the following
		speficications:

		1. The course to archive is determined by the ID passed in the URL
		2. Route must have token verification middleware
		3. Only admins must be allowed to archive courses
		4. Courses are NOT actually deleted, only their isActive fields changed from true to false

	*/
	if(auth.decode(req.headers.authorization).isAdmin) {
		courseController.archivedCourse(req.params).then(resultFromController => res.send (
		resultFromController))
	}else{
		res.send({auth: "failed"})
	}

})




module.exports = router;